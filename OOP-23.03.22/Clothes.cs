﻿namespace OOP_23._03._22
{
    internal class Clothes : Product
    {
        public Size Size { get; set; }

        public string Color { get; set; }

        public Clothes(string name, string brand, decimal price, Size size, string color) 
            : base(name, brand, price)
        {
            Size = size;
            Color = color;
        }

        public override decimal CalculateDiscount(DateTime dateOfPurchase)
        {
            if (dateOfPurchase.DayOfWeek >= DayOfWeek.Monday 
                && dateOfPurchase.DayOfWeek <= DayOfWeek.Friday)
            {
                return 0.1m;
            }

            return base.CalculateDiscount(dateOfPurchase);
        }
    }
}

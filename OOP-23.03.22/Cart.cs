﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_23._03._22
{
    internal class Cart
    {
        public Dictionary<Product, int> ProductQuantity { get; set; }

        public Cart()
        {
            ProductQuantity = new Dictionary<Product, int>();
        }
    }
}

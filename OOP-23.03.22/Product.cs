﻿namespace OOP_23._03._22
{
    internal abstract class Product
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public decimal Price { get; set; }

        public Product(string name, string brand, decimal price)
        {
            Name = name;
            Brand = brand;
            Price = price;
        }

        public virtual decimal CalculateDiscount(DateTime dateOfPurchase)
        {
            return 0;
        }
    }
}

﻿namespace OOP_23._03._22
{
    internal abstract class PerishableProduct : Product
    {
        public DateTime ExpirationDate { get; set; }
        protected PerishableProduct(string name, string brand, decimal price, DateTime expirationDate) 
            : base(name, brand, price)
        {
            ExpirationDate = expirationDate;
        }

        public override decimal CalculateDiscount(DateTime dateOfPurchase)
        {
            if ((ExpirationDate.Day - dateOfPurchase.Day) == 0)
            {
                return 0.5m;
            }

            if ((ExpirationDate.Day - dateOfPurchase.Day) < 5)
            {
                return 0.1m;
            }

            return base.CalculateDiscount(dateOfPurchase);
        }
    }
}

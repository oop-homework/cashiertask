﻿namespace OOP_23._03._22
{
    internal class Appliance : Product
    {
        public string Model { get; set; }

        public DateTime ProductionDate { get; set; }

        public double Weight { get; set; }

        public Appliance(string name, string brand, decimal price, string model, DateTime prodDate, double weight) 
            : base(name, brand, price)
        {
            Model = model;
            ProductionDate = prodDate;
            Weight = weight;
        }

        public override decimal CalculateDiscount(DateTime dateOfPurchase)
        {
            if ((dateOfPurchase.DayOfWeek == DayOfWeek.Sunday 
                || dateOfPurchase.DayOfWeek == DayOfWeek.Saturday) && Price > 999)
            {
                return 0.05m;
            }

            return base.CalculateDiscount(dateOfPurchase);
        }
    }
}

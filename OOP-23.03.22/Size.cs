﻿namespace OOP_23._03._22
{
    internal enum Size
    {
        XS,
        S,
        M,
        L,
        XL
    }
}

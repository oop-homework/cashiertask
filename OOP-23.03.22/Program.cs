﻿// See https://aka.ms/new-console-template for more information

using OOP_23._03._22;

Food apples = new Food("apples", "brandApples", 2.45m, DateTime.Parse("29.03.2022"));
Beverage milk = new Beverage("milk", "brandMilk", 0.99m, DateTime.Parse("28.03.2022"));
Clothes tshirt = new Clothes("tshirt", "brandShirt", 15.99m, Size.M, "violet");
Appliance laptop = new Appliance("laptop", "brandLaptop", 2345, "modelLaptop", DateTime.Now, 24);
Console.WriteLine();

Console.WriteLine();
Cart cart = new Cart();
cart.ProductQuantity.Add(apples, 2);
cart.ProductQuantity.Add(milk, 2);
cart.ProductQuantity.Add(tshirt, 2);
cart.ProductQuantity.Add(laptop, 1);
Cashier.PrintReceipt(cart, DateTime.Now);

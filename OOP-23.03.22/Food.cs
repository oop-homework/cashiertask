﻿namespace OOP_23._03._22
{
    internal class Food : PerishableProduct
    {
        public Food(string name, string brand, decimal price, DateTime expirationDate) 
            : base(name, brand, price, expirationDate)
        {
        }
    }
}

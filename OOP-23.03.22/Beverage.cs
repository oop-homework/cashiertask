﻿namespace OOP_23._03._22
{
    internal class Beverage : PerishableProduct
    {
        public Beverage(string name, string brand, decimal price, DateTime expirationDate)
            : base(name, brand, price, expirationDate)
        {
        }
    }
}

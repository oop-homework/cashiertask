﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_23._03._22
{
    internal class Cashier
    {
        public static void PrintReceipt(Cart cart, DateTime dateOfPurchase)
        {
            Console.WriteLine($"Date: {dateOfPurchase.Date}");
            Console.WriteLine("Products:");

            decimal subTotal = 0;
            decimal totalDiscount = 0;

            foreach (var product in cart.ProductQuantity.Keys)
            {
                int productQuantity = cart.ProductQuantity[product];
                decimal productPrice = productQuantity * product.Price;
                decimal productDiscountPercent = product.CalculateDiscount(dateOfPurchase) * 100;
                decimal productDiscountValue = product.CalculateDiscount(dateOfPurchase) * productPrice;

                subTotal += productPrice;
                totalDiscount += productDiscountValue;

                Console.WriteLine($"{product.Name} - {product.Brand}");
                Console.WriteLine($"{productQuantity} x ${product.Price:F2} = {productPrice:F2}");
                Console.WriteLine($"#discount {productDiscountPercent:F2}% -${productDiscountValue:F2}");
                Console.WriteLine();
            }

            Console.WriteLine($"Subtotal: {subTotal:F2}");
            Console.WriteLine($"Discount: -${totalDiscount:F2}");
            Console.WriteLine($"Total: {subTotal - totalDiscount:F2}");
        }
    }
}
